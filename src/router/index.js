import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Login',
    component: () => import(/* webpackChunkName: "about" */ '../views/login/ViewLogin.vue'),
    meta: {
      title: 'Login | emprenD',
      authRequired: false,
    }
  },
  {
    path: '/singup',
    name: 'SingUp',
    component: () => import(/* webpackChunkName: "about" */ '../views/login/ViewSingUp.vue'),
    meta: {
      title: 'Sign Up | emprenD',
      authRequired: false,
    }
  },
  {
    path: '/create-idea',
    name: 'CreateIdea',
    component: () => import(/* webpackChunkName: "about" */ '../views/idea/ViewIdeaCreate.vue'),
    meta: {
      title: 'Create Idea | emprenD',
      authRequired: true,
    }
  }
]

const router = new VueRouter({
  routes
})

router.beforeEach((routeTo, routeFrom, next) => {

  const authRequired = routeTo.matched.some(route => route.meta.authRequired);

  if (!authRequired) return next();

  if(localStorage.getItem('token')) return next();

   return next({
     name: 'Login',
   });

});

router.afterEach((to) => {
  Vue.nextTick(() => {
    document.title = to.meta.title;
  });
});

export default router
