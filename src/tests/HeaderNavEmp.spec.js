import { expect } from "chai";
// The path is relative to the project root.
import HeaderNavEmp from "@/components/headers/HeaderNavEmp";
import {shallowMount} from "@vue/test-utils";

// eslint-disable-next-line no-undef
describe('HeaderNavEmp.vue', () => {
    // eslint-disable-next-line no-undef
    it(`should render propValue as its text content`, () => {
        // Extend the component to get the constructor, which we can then initialize directly.
        let component = shallowMount(HeaderNavEmp,
            {
                propsData: {
                    name:'Test'
                }
            }
        );

        expect(component.find('.name').text())
            .to.equal('Test');
    });
});