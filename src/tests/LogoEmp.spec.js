import { expect } from "chai";
// The path is relative to the project root.
import {shallowMount} from "@vue/test-utils";
import LogoEmp from "@/components/titles/LogoEmp";

// eslint-disable-next-line no-undef
describe('LoginEmp.vue', () => {
    // eslint-disable-next-line no-undef
    it(`should render propValue as its text content`, () => {
        // Extend the component to get the constructor, which we can then initialize directly.
        let component = shallowMount(LogoEmp,
            {
                propsData: {
                    title:'emprenD'
                }
            }
        );
        expect(component.find('.text').text())
            .to.equal('emprenD');
    });
});