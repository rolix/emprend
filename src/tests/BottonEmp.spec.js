import { expect } from "chai";
import Vue from 'vue';
// The path is relative to the project root.
import BottonEmp from "@/components/buttons/BottonEmp";

// eslint-disable-next-line no-undef
describe('BottonEmp.vue', () => {
    // eslint-disable-next-line no-undef
    it(`should render propValue as its text content`, () => {
        // Extend the component to get the constructor, which we can then initialize directly.
        const Constructor = Vue.extend(BottonEmp);

        const comp = new Constructor({
            propsData: {
                title: 'Test'
            }
        }).$mount();

        expect(comp.$el.textContent)
            .to.equal('Test');
    });
});