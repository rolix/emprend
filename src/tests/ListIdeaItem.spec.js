import { expect } from "chai";
// The path is relative to the project root.
import ListIdeaItem from "@/components/lists/ListIdeaItem";
import {shallowMount} from "@vue/test-utils";

// eslint-disable-next-line no-undef
describe('HeaderNavEmp.vue', () => {
    // eslint-disable-next-line no-undef
    it(`should render propValue as its text content`, () => {
        // Extend the component to get the constructor, which we can then initialize directly.
        let component = shallowMount(ListIdeaItem,
            {
                propsData: {
                    text:'Test',
                    date:'1988/20/10',
                    userId:1
                }
            }
        );
        expect(component.find('.cont-tex').text())
            .to.equal('Test');
    });
});