import { expect } from "chai";
// The path is relative to the project root.
import TitleSubline from "@/components/titles/TitleSubline";
import {shallowMount} from "@vue/test-utils";

// eslint-disable-next-line no-undef
describe('TitleSubline.vue', () => {
    // eslint-disable-next-line no-undef
    it(`should render propValue as its text content`, () => {
        // Extend the component to get the constructor, which we can then initialize directly.
        let component = shallowMount(TitleSubline,
            {
                propsData: {
                    title:'Test'
                }
            }
        );
        expect(component.find('.title').text())
            .to.equal('Test');
    });
});