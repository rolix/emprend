export const getters = {
  isLoading: state => state.loading.visible,
  messageLoading: state => state.loading.message,
  isSnackbar: state => state.snackbar.visible,
  messageSnakbar: state => state.snackbar.message,
  isBeterLoading: state => state.loadingBeter.visible
};
