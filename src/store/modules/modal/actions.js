import {
  LOADING,
  SET_MESSAGE_LOADING,
  OPEN_SNACKBAR,
  SET_MESSAGE_SNACKBAR, LOADING_BETER,
} from './mutations-types';

export const actions = {
  setLoading: ({ commit }, payload) => {
    commit(LOADING, payload.visible);
    commit(SET_MESSAGE_LOADING, payload.message || 'Cargando...');
  },
  setLoadingBeter: ({ commit }, payload) => {
    commit(LOADING_BETER, payload.visible);
  },
  setSnackbar: ({ commit }, payload) => {
    commit(OPEN_SNACKBAR, payload.visible);
    commit(SET_MESSAGE_SNACKBAR, payload.message);
  },
  handleErrors: ({ commit }, payload) => {
    if (payload.response) {
      if (payload.response.data.Mensaje) {
        commit(SET_MESSAGE_SNACKBAR, payload.response.data.Mensaje);
        commit(OPEN_SNACKBAR, true);
      }
    } else if (payload.request) {
      // console.log(payload.request);
    } else {
      // console.log('Error', payload.message);
    }
  },
};
