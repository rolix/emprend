import {
  LOADING,
  SET_MESSAGE_LOADING,
  OPEN_SNACKBAR,
  SET_MESSAGE_SNACKBAR,
  LOADING_BETER
} from './mutations-types';

export const mutations = {

  [LOADING](state, payload) {
    if (typeof payload !== 'boolean') return;
    state.loading.visible = payload;
  },
  [LOADING_BETER](state, payload) {
    if (typeof payload !== 'boolean') return;
    state.loadingBeter.visible = payload;
  },
  [SET_MESSAGE_LOADING](state, payload) {
    state.loading.message = payload;
  },
  [OPEN_SNACKBAR](state, payload) {
    if (typeof payload !== 'boolean') return;
    state.snackbar.visible = payload;
  },
  [SET_MESSAGE_SNACKBAR](state, payload) {
    state.snackbar.message = payload;
  },
};
