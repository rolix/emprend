export const HORARIO_SEMANAL_NORMAL = [
  {
    position: 1,
    title: 'Lunes',
    value: [
      {
        minute: '00',
        hour: '08',
        time: 'AM',
      },
      {
        minute: '00',
        hour: '06',
        time: 'PM',
      },
    ],
  },
  {
    position: 2,
    title: 'Martes',
    value: [
      {
        minute: '00',
        hour: '08',
        time: 'AM',
      },
      {
        minute: '00',
        hour: '06',
        time: 'PM',
      },
    ],
  },
  {
    position: 3,
    title: 'Miércoles',
    value: [
      {
        minute: '00',
        hour: '08',
        time: 'AM',
      },
      {
        minute: '00',
        hour: '06',
        time: 'PM',
      },
    ],
  },
  {
    position: 4,
    title: 'Jueves',
    value: [
      {
        minute: '00',
        hour: '08',
        time: 'AM',
      },
      {
        minute: '00',
        hour: '06',
        time: 'PM',
      },
    ],
  },
  {
    position: 5,
    title: 'Viernes',
    value: [
      {
        minute: '00',
        hour: '08',
        time: 'AM',
      },
      {
        minute: '00',
        hour: '06',
        time: 'PM',
      },
    ],
  },
];

export const DIAS_SEMANA = [
  "Lunes",
  "Martes",
  "Miércoles",
  "Jueves",
  "Viernes"
]

export const ROLES = {
  PUBLIC: 'PUBLIC',
  MEDICO: 'MEDICO',
  PACIENTE: 'PACIENTE',
};

export const TIPO_TELEFONO = {
  CASA: 'CS',
  CELULAR: 'CEL',
  FAX: 'FX',
  CORREO: 'CO',
};


export const TIPO_RED_SOCIAL = {
  FACEBOOK: 'FB',
  GOOGLE: 'GO'
};


export const TIPO_CONF_EXTRA = {
  SERVICIO: 'SER',
  EXPERIENCIA: 'EXP',
  FORMACION: 'FOR'
};


export const ESTATUS_CITA = {
   CONFIRMADA : 'CE',
   CANCELADA : 'CA',
   AGENDADA : 'AG',
  PORCONFIRMAR : 'CO',
  NO_ENCONTRADA: 'NO'
};


export const TYPE_PAY = {
  FREE : 'FREE',
  MONTH : 'PRO_MON',
  YEAR : 'PRO_ANN',
};

