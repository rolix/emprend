import { helpers } from 'vuelidate/lib/validators';

export const almostOneNumber = helpers.regex('almostOneNumber', /\d+/);
export const alphaWithSpaces = helpers.regex(
  'alphaWithSpaces',
  /^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$/
);
