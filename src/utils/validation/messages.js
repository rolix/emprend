export const MESSAGE_VALIDATION = {
  REQUIRED: 'El campo es obligatorio',
  EMAIL_FORMAT: 'El formato del correo es incorrecto',
  MAXLENGTH: 'Se superó la longitud del campo',
  ALMOST_ONE_NUMBER: 'Al menos debe contener un número',
  MINLENGTH: 'Los caracteres no superan la longitud mínima',
  ALPHA: 'Sólo se aceptan valores alfabéticos',
  SAME_AS_PASSWORD: 'Los campos no son iguales',
};
