import { MESSAGE_VALIDATION } from './messages';
import { almostOneNumber, alphaWithSpaces } from './custom-validations';

export { MESSAGE_VALIDATION, almostOneNumber, alphaWithSpaces };
