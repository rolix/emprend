import AuthService from './AuthService';
import IdeaService from "@/service/IdeaService";

const services = {
  auth: AuthService,
  idea: IdeaService
};

/**
 * @author: Orlando Martinez
 * @desc: Conjunto de funciones útiles para los repositories
 */
export const RepositoryFactory = {
  /**
   * @author: Orlando Martinez
   * @desc: Obtiene el Repository por el nombre
   * @param {string} nombre Nombre del Repository
   * @return {Repository} Devuelve una instancia de un Repository
   */
  get: nombre => services[nombre],
};
