import Service from './Service';

export default {
  signup: user => Service.post(`/api/register`, user),
  login: user => Service.post(`/api/login`, user),
  getUser: (idUser) => Service.get(`/api/user/${idUser}`),
  logout: () => Service.post(`/api/logout`),
};
