/**
 * @author: Orlando Martinez
 * @description: Instacia de Axios para peticiones HTTP  https://github.com/axios/axios#custom-instance-defaults
 */
import axios from 'axios';

const instance = axios.create({
  baseURL: process.env.VUE_APP_API ? process.env.VUE_APP_API : 'http://localhost:8000'
});

// Modifica los headers en todas las peticiones
// Más información: https://github.com/axios/axios#interceptors
instance.interceptors.request.use(config => {
  // // Obtiene el token del localstore, si no existe lo setea como null
  const TOKEN = localStorage.getItem('token')
      ? localStorage.getItem('token').toString()
      : null;

// Agrega el prefijo al JWT token obtenido
  const BEARER_TOKEN = 'Bearer ' + TOKEN;
  config.headers['Authorization'] = BEARER_TOKEN;
  config.headers['Access-Control-Allow-Origin'] = '*';
  return config;
});

// Recupera la respuesta de cualquier petición
// Más información: https://github.com/axios/axios#interceptors

export default instance;
