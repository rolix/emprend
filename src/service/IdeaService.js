import Service from './Service';

export default {
  allIdeas: () => Service.get(`/api/comments`),
  getIdeasByPage: (page) => Service.get(`/api/comments?page=${page}`),
  saveIdea: (model) => Service.post(`/api/comments`, model),
};
